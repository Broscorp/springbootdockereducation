FROM openjdk:17-alpine3.14

WORKDIR /app

COPY target/getters-0.0.1-SNAPSHOT.jar service.jar

CMD ["java", "-jar", "service.jar"]