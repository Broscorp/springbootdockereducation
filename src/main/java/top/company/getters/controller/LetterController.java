package top.company.getters.controller;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class LetterController {

    private static String text = "Hello, world";

    @GetMapping("/get/letter")
    public String getLetter() {
        return text;
    }

}
